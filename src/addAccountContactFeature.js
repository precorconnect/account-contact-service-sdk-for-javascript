import {inject} from 'aurelia-dependency-injection';
import AccountContactServiceSdkConfig from './accountContactServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import AddAccountContactReq from './addAccountContactReq';
import AccountContactId from './accountContactId';

@inject(AccountContactServiceSdkConfig, HttpClient)
class AddAccountContactFeature {

    _config:AccountContactServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * @param {AddAccountContactReq} request
     * @param {string} accessToken
     * @returns {Promise|Promise.<AccountContactId>}
     */
    execute(request:AddAccountContactReq,
            accessToken:string):Promise<AccountContactId> {

        return this._httpClient
            .createRequest('account-contacts')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            //.withHeader('Accept', 'text/plain')
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then((response) => response.content);
    }
}

export default AddAccountContactFeature;