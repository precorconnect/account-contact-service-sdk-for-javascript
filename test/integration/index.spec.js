import AccountContactServiceSdk,{AddAccountContactReq,AccountContactServiceSdkConfig} from '../../src/index';
import AccountContactView from '../../src/accountContactView';
import config from './config';
import factory from './factory';
import dummy from '../dummy';
import jwt from 'jwt-simple';

/*
 test methods
 */
describe('Index module', () => {

    /*
     constants
     */
    describe('default export', () => {
        it('should be AccountContactServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new AccountContactServiceSdk(config.accountContactServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(AccountContactServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addAccountContact method', () => {
            it('should return AccountContactId', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new AccountContactServiceSdk(config.accountContactServiceSdkConfig);

                /*
                 act
                 */
                const accountContactIdPromise =
                    objectUnderTest.addAccountContact(
                        new AddAccountContactReq(
                            dummy.accountId,
                            dummy.firstName,
                            dummy.lastName,
                            dummy.phoneNumber,
                            dummy.emailAddress,
                            dummy.iso31661Alpha2Code,
                            dummy.iso31662Code
                        ),
                        factory.constructValidPartnerRepOAuth2AccessToken(
                            config.idOfExistingPartnerAccountAssociation.partnerAccountId
                        )
                    );

                /*
                 assert
                 */
                accountContactIdPromise
                    .then(accountContactId => {
                        expect(accountContactId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });
        describe('getAccountContactWithId method', () => {
            it('should return expected AccountContactView', (done) => {
                /*
                 arrange
                 */
                let expectedAccountContactView;

                const objectUnderTest =
                    new AccountContactServiceSdk(config.accountContactServiceSdkConfig);

                // seed a new accountContact so we can test the retrieval of it
                const addAccountContactRequest =
                    factory.constructValidAddAccountContactRequest(
                        config.idOfExistingPartnerAccountAssociation.accountId
                    );

                const expectedAccountContactIdPromise =
                    objectUnderTest
                        .addAccountContact(
                            addAccountContactRequest,
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                config.idOfExistingPartnerAccountAssociation.partnerAccountId
                            )
                        )
                        .then(expectedAccountContactId => {
                            // construct expected account contact view
                            expectedAccountContactView =
                                new AccountContactView(
                                    addAccountContactRequest.accountId,
                                    expectedAccountContactId.id,
                                    addAccountContactRequest.firstName,
                                    addAccountContactRequest.lastName,
                                    addAccountContactRequest.phoneNumber,
                                    addAccountContactRequest.emailAddress
                                );
                            return expectedAccountContactId;
                            }
                        );

                /*
                 act
                 */
                const actualAccountContactViewPromise =
                    expectedAccountContactIdPromise
                        .then(expectedAccountContactId => {

                                // get accountContact
                                return objectUnderTest.getAccountContactWithId(
                                    expectedAccountContactId.id,
                                    factory.constructValidPartnerRepOAuth2AccessToken(
                                        config.idOfExistingPartnerAccountAssociation.partnerAccountId
                                    )
                                )
                            }
                        );

                /*
                 assert
                 */
                actualAccountContactViewPromise
                    .then((actualAccountContactView) => {
                        expect(actualAccountContactView).toEqual(expectedAccountContactView);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('listAccountContactsWithAccountId', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new AccountContactServiceSdk(config.accountContactServiceSdkConfig);

                /*
                 act
                 */
                const accountContactSynopsisPromise =
                    objectUnderTest
                        .listAccountContactsWithAccountId(
                            config.idOfExistingPartnerAccountAssociation.accountId,
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                config.idOfExistingPartnerAccountAssociation.partnerAccountId
                            )
                        );

                /*
                 assert
                 */
                accountContactSynopsisPromise
                    .then((accountContactSynopsis) => {
                        expect(accountContactSynopsis.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));
            },20000);
        });

    });
});