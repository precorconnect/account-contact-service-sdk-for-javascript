import AccountContactServiceSdkConfig from '../../src/accountContactServiceSdkConfig';

export default {
    accountContactServiceSdkConfig: new AccountContactServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    idOfExistingPartnerAccountAssociation: {
		accountId: '001A000001DcxdmIAB',
		partnerAccountId: '001A000000MGmkCIAT'
    }
};

